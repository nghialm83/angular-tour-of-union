import { Component, OnInit } from '@angular/core';
import { Hero } from '../model/hero';
import { HEROES } from '../service/mock-heroes';
import { FoodService } from '../service/food-service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  hero : Hero = {
    id: 1,
    name: 'Geisha'
  };

  heroes = HEROES;
  foodsrv: FoodService;
  public foods;

  constructor() { }

  ngOnInit() {
    this.getFoods();
  }

  getFoods() {
    this.foodsrv.getFoods().subscribe(
      data => {this.foods},
      err => console.log(err),
      () => console.log('Get foods is done')
    );
  }

  selectedHero: Hero;

  onSelect(hero: Hero): void
  {
    this.selectedHero = hero;
    console.log('Select item');
  }

}
