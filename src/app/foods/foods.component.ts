import { Component, OnInit } from '@angular/core';
import { FoodService } from '../service/food-service';
// import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-foods',
  templateUrl: './foods.component.html',
  styleUrls: ['./foods.component.css']
})
export class FoodsComponent implements OnInit {
  public foods;

  constructor(private _FoodSer: FoodService) { }

  ngOnInit() {
    //this.getFoods();
  }

  getFoods() {
    this._FoodSer.getFoods().subscribe(
      data => {this.foods},
      err => console.log(err),
      () => console.log('Get foods is done')
    );
  }


}
