import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable()
export class FoodService{
    constructor (private http:HttpClient) {}

    public getFoods(){
        return this.http.get('http://localhost:3000/api/food');
    }
}